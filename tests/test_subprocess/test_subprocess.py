import asyncio
import contextlib
import sys
from typing import Callable, Coroutine

import pytest

from asyncio_toolbox._helpers import is_windows
from asyncio_toolbox.command_execution import Command, Process


@pytest.fixture(
    params=[
        pytest.param(Process.interrupt, id="interrupt"),
        pytest.param(Process.kill, id="kill"),
        pytest.param(Process.terminate, id="terminate"),
    ],
)
def kill_action(request):
    return request.param


@contextlib.asynccontextmanager
async def start_process(command: Command, *, start_time: float = 0):
    process = await Process.from_command(command)
    if start_time:
        with pytest.raises(asyncio.TimeoutError):
            await process.wait(timeout=start_time)
    try:
        yield process
    finally:
        await process.terminate()
        await process.wait()


async def test_basic_execution(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "1", "0"])
    async with start_process(command) as process:
        await asyncio.sleep(1)
        assert process.returncode == 0
        assert process._wait_process_task.done()


async def test_script_interrupt(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "1"])
    async with start_process(command, start_time=1) as process:
        await process.interrupt()
        await process.wait(timeout=5)
        if is_windows:
            assert process.returncode == 4294967295
        else:
            assert process.returncode == 1

        assert process.stdout[-2] == "STOPPED BY CTRL-C"
        assert process.stdout[-1] == "ATEXIT"


async def test_script_multiple_interrupt(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "1", "1"])
    async with start_process(command, start_time=1) as process:

        await process.interrupt()
        with pytest.raises(asyncio.TimeoutError):
            await process.wait(timeout=1)

        await process.interrupt()
        await process.wait(timeout=1)

        if is_windows:
            assert process.returncode == 4294967295
        else:
            assert process.returncode == 1

        assert "STOPPED BY CTRL-C" in process.stdout[:-3]
        assert process.stdout[-2] == "STOPPED BY CTRL-C"
        assert process.stdout[-1] == "ATEXIT"


async def test_script_kill(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "1", "1"])
    async with start_process(command, start_time=1) as process:
        await process.kill()
        await process.wait(timeout=1)
        if is_windows:
            assert process.returncode == 1
        else:
            assert process.returncode == -9


async def test_script_terminate(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "1", "1"])
    async with start_process(command, start_time=1) as process:
        await process.terminate()
        await process.wait(timeout=1)
        if is_windows:
            assert process.returncode == 1
        else:
            assert process.returncode == -15


async def test_multiple_script_stop(stoppable_script_path, kill_action):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "1"])
    async with contextlib.AsyncExitStack() as stack:
        processes_f, _ = await asyncio.wait(
            [
                stack.enter_async_context(start_process(command, start_time=1))
                for _ in range(10)
            ],
            return_when=asyncio.FIRST_EXCEPTION,
        )
        processes = [pf.result() for pf in processes_f]
        await kill_action(processes[0])
        await processes[0].wait(1)
        await asyncio.sleep(1)
        assert all([p.running for p in processes[1:]])


async def test_script_stdout_capture(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "10", "0"])
    async with start_process(command) as process:
        await process.wait()
        assert process.stdout == list(map(str, range(10))) + ["ATEXIT"]
        assert process._stderr_reader_task.done()
        assert process._stdout_reader_task.done()
        assert process._wait_process_task.done()


@pytest.fixture()
def ping_command() -> Command:
    if is_windows:
        return Command(args=["ping", "-t", "127.0.0.1"])
    else:
        return Command(args=["ping", "127.0.0.1"], merge_outputs=True)


async def test_ping(ping_command, kill_action: Callable[[Process], Coroutine]):
    async with start_process(ping_command, start_time=1) as process:
        await kill_action(process)
        try:
            await process.wait(timeout=5)
        except asyncio.TimeoutError:
            print(process.stdout)
            raise


async def test_command_not_found():
    with pytest.raises(OSError):
        async with start_process(Command(args=["'"]), start_time=1) as process:
            pass


async def test_wait_all_workers(stoppable_script_path):
    command = Command(args=[sys.executable, stoppable_script_path, "100000", "0"])
    async with start_process(command) as process:
        await process.wait_for_all_workers()
        assert process.stdout == list(map(str, range(100000))) + ["ATEXIT"]
