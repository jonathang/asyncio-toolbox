from pathlib import Path

import pytest


@pytest.fixture(scope="session")
def scripts_dir() -> Path:
    return Path(__file__).parent / "scripts"


@pytest.fixture(scope="session")
def stoppable_script_path(scripts_dir) -> Path:
    return scripts_dir / "stoppable.py"
