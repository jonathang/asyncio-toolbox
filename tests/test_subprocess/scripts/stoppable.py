import sys
import time
import atexit


loops = int(sys.argv[1])
delay = float(sys.argv[2])
try:
    allowed_interrupts = int(sys.argv[3])
except IndexError:
    allowed_interrupts = 0

atexit.register(lambda *args: print("ATEXIT"))

while allowed_interrupts >= 0:
    try:
        for i in range(loops):
            print(i)
            time.sleep(delay)
    except KeyboardInterrupt:
        allowed_interrupts -= 1
        print("STOPPED BY CTRL-C")
        continue
    break
else:
    exit(-255)
exit(0)
