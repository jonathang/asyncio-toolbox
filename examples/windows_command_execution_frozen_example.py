import asyncio
from pathlib import Path

from asyncio_toolbox._helpers import is_frozen
from asyncio_toolbox.command_execution import (
    Command,
    Process,
    set_windows_command_wrapper_args,
)
from asyncio_toolbox.command_execution.windows_command_wrapper import (
    wrap_windows_command_in_python_process,
)


if __name__ == "__main__":

    import sys

    try:
        if sys.argv[1] == "slave":
            sys.argv = sys.argv[1:]
            print("starting slave process")
            sys.exit(
                wrap_windows_command_in_python_process(sys.argv[2:], Path(sys.argv[1]))
            )
    except IndexError:
        pass

    print("running as master process")

    if is_frozen:
        print(
            "running in frozen mode, using our current executable with entrypoint slave"
        )
        set_windows_command_wrapper_args([sys.executable, "slave"])
    else:
        print("running as python, no need to cheat")

    command = Command(args=["ping", "-t", "127.0.0.1"], merge_outputs=True)

    async def main():
        p = await Process.from_command(command)
        await p.kill()
        try:
            await p.wait(1)
            print("process stopped")
            print("\n".join(p.stdout))
            print("\n".join(p.stderr))
        except asyncio.TimeoutError:
            print("could not kill process, killing it")
            await p.terminate()
            await p.wait()
            print("\n".join(p.stdout))
            print("\n".join(p.stderr))
            raise

    asyncio.set_event_loop_policy(asyncio.WindowsProactorEventLoopPolicy())
    asyncio.run(main())
