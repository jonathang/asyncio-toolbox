from setuptools import find_packages, setup


setup(
    name="asyncio_toolbox",
    use_scm_version=True,
    description="Asyncio tools and helpers",
    author="Jonathan GAYVALLET",
    author_email="jonathan.mael.gayvallet@gmail.com",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    package_data={"asyncio_toolbox": ["py.typed"],},
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "License :: OSI Approved :: MIT License",
        "Intended Audience :: Developers",
        "Typing :: Typed",
    ],
    python_requires=">=3.6",
    setup_requires=["setuptools_scm"],
    install_requires=["attrs", "typing-extensions"],
    extras_require={':sys_platform == "win32"': ["pywin32"],},
)
